package comm.unism.succ;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.plugin.java.JavaPlugin;

public final class CommunityMotivatorPlugin extends JavaPlugin {

    @Override
    public void onEnable() {
        Bukkit.getServer().broadcastMessage("SUCC Community Motivator is now enabled");
    }

    @Override
    public void onDisable() {
        Bukkit.getServer().broadcastMessage("You stupid capitalists disabled the Community Motivator");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (command.getName().equals("accuse")) {
            if (sender instanceof Player) {
                if (args.length == 0) {
                    Player player = (Player) sender;
                    TNTPrimed tnt = (TNTPrimed) player.getWorld().spawnEntity(player.getLocation(), EntityType.PRIMED_TNT);
                    tnt.setFuseTicks(0);
                    Bukkit.getServer().broadcastMessage(player.getName() + " was accused...");
                    return true;
                }
                else {
                    Player player = Bukkit.getServer().getPlayer(args[0]);
                    TNTPrimed tnt = (TNTPrimed) player.getWorld().spawnEntity(player.getLocation(), EntityType.PRIMED_TNT);
                    tnt.setFuseTicks(0);
                    Bukkit.getServer().broadcastMessage(player.getName() + " was accused...");
                    return true;
                }
            }
        }
        return true;
    }
}
